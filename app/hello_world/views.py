from django.views.generic import TemplateView, CreateView
from .models import YummyFood
from .forms import YummyForm

class Index(TemplateView):
    template_name = "index.html"

class FoodOrder(CreateView):
    model = YummyFood
    form_class = YummyForm
    template_name = "yummy.html"

