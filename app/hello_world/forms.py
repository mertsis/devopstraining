from django import forms
from django.forms import ModelForm

from .models import YummyFood


class DateInput(forms.DateInput):
    input_type = 'date'


class YummyForm(ModelForm):

    class Meta:
        model = YummyFood
        fields = "__all__"
        widgets = {
            'order_date': DateInput(),
        }