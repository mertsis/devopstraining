from django.db import models


class YummyFood(models.Model):
    food_name = models.CharField(max_length=50)
    material = models.CharField(max_length=20)
    order_date = models.DateTimeField()
